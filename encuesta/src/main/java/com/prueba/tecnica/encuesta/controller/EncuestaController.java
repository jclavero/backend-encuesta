package com.prueba.tecnica.encuesta.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.tecnica.encuesta.dto.ResultadosResponseDto;
import com.prueba.tecnica.encuesta.model.Encuesta;
import com.prueba.tecnica.encuesta.service.EncuestaService;

@RestController
public class EncuestaController {

	@Autowired
	private EncuestaService encuestaService;
		 
    @PostMapping("/encuesta")
    public Encuesta saveEncuesta(@RequestBody Encuesta encuesta)
    {
        return encuestaService.saveEncuesta(encuesta);
    }
 
   
    @GetMapping("/resultados")
    public List<ResultadosResponseDto> getResultados()
    {
        return encuestaService.getResultados();
    }
}
