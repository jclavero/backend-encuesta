package com.prueba.tecnica.encuesta.dto;

public class EncuestaDto {

	private String correo;
	private String estilo;
	
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getEstilo() {
		return estilo;
	}
	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}
}
