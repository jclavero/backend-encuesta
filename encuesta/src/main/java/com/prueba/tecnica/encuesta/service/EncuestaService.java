package com.prueba.tecnica.encuesta.service;

import java.util.List;

import com.prueba.tecnica.encuesta.dto.ResultadosResponseDto;
import com.prueba.tecnica.encuesta.model.Encuesta;

public interface EncuestaService {

    Encuesta saveEncuesta(Encuesta encuesta);

    List<ResultadosResponseDto> getResultados();
    
}
