package com.prueba.tecnica.encuesta.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.prueba.tecnica.encuesta.model.Encuesta;

public interface EncuestaRepository extends CrudRepository<Encuesta, Integer>{

	    @Query("SELECT COUNT(u) FROM Encuesta u WHERE u.estilo=:estilo")
	    long count(@Param("estilo") String estilo);
	
}
