package com.prueba.tecnica.encuesta.dto;

public class ResultadosResponseDto {

	private String estilo;
	private Long total;
	
	public String getEstilo() {
		return estilo;
	}
	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}
	public Long getTotal() {
		return total;
	}
	public void setTotal(Long total) {
		this.total = total;
	}	
	
}
