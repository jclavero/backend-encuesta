package com.prueba.tecnica.encuesta.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.tecnica.encuesta.dto.ResultadosResponseDto;
import com.prueba.tecnica.encuesta.model.Encuesta;
import com.prueba.tecnica.encuesta.repository.EncuestaRepository;

@Service
public class EncuestaServiceImpl implements EncuestaService{

	@Autowired
	private EncuestaRepository encuestaRepository;
	
	@Override
	public Encuesta saveEncuesta(Encuesta encuesta) {
		return encuestaRepository.save(encuesta);
	}

	@Override
	public List<ResultadosResponseDto> getResultados() {
		List<String> estilos = Arrays.asList("rock","pop","jazz","clasic","etc");
		List<ResultadosResponseDto> resultado = new ArrayList<>();
		for(String estilo:estilos) {
			ResultadosResponseDto obj = new ResultadosResponseDto();
			obj.setEstilo(estilo);
			obj.setTotal(encuestaRepository.count(estilo));
			resultado.add(obj);
		}
		return resultado;
	}

}
