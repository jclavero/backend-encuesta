package com.prueba.tecnica.encuesta;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.prueba.tecnica.encuesta.model.Encuesta;
import com.prueba.tecnica.encuesta.repository.EncuestaRepository;
import com.prueba.tecnica.encuesta.service.EncuestaServiceImpl;

@SpringBootTest
public class EncuestaServiceTests {

	@Mock
	private EncuestaRepository encuestaRepository;
	
	@InjectMocks
	private EncuestaServiceImpl encuestaService;

	private Encuesta encuesta;
	
    @BeforeEach
    public void init(){
       MockitoAnnotations.openMocks(encuestaRepository);
       encuesta = new Encuesta();
       encuesta.setCorreo("juan@gmail.com");
       encuesta.setEstilo("rock");
    }
    
    @Test
    void saveEncuestaTest(){
    	when(encuestaRepository.save(any())).thenReturn(encuesta);
    	assertNotNull(encuestaService.saveEncuesta(encuesta));
    }
    
    @Test
    void getResultados(){
    	when(encuestaRepository.count()).thenReturn(anyLong());
    	assertNotNull(encuestaService.getResultados());
    }
    
	
	
	
	
}
