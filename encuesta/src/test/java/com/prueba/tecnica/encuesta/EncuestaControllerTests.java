package com.prueba.tecnica.encuesta;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.prueba.tecnica.encuesta.controller.EncuestaController;
import com.prueba.tecnica.encuesta.dto.ResultadosResponseDto;
import com.prueba.tecnica.encuesta.model.Encuesta;
import com.prueba.tecnica.encuesta.service.EncuestaService;

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc(addFilters = false)
@WebMvcTest(controllers = EncuestaController.class)
public class EncuestaControllerTests {

    @Autowired
    private MockMvc mockMvc;
    
    @MockBean
    private EncuestaService encuestaService;
    
    @Autowired
    private ObjectMapper mapper;
    
    @Test
    void saveEncuestaTest() throws Exception {
        var result = new Encuesta();
        when(encuestaService.saveEncuesta(any())).thenReturn(result);
        mockMvc.perform(MockMvcRequestBuilders
                        .post("/encuesta")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(result)))
                .andDo(print())
                .andExpect(status().isOk());
    }
    
    @Test
    void getResultadosTest() throws Exception {
        var result = List.of(new ResultadosResponseDto());
        when(encuestaService.getResultados()).thenReturn(result);
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/resultados"))
                .andDo(print())
                .andExpect(status().isOk());
    }
    
}
